import { Component, OnInit } from '@angular/core';
import '@material/list';

@Component({
  selector: 'app-cliente-top',
  templateUrl: './cliente-top.component.html',
  styleUrls: ['./cliente-top.component.scss']
})
export class ClienteTopComponent implements OnInit {

  public map: any = { lat: 51.678418, lng: 7.809007 };
  public chart1Type:string = 'bar';
  public chart2Type:string = 'pie';
  public chart3Type:string = 'line';
  public chart4Type:string = 'radar';
  public chart5Type:string = 'doughnut';


  public chartType = 'line';

  /*public chartDatasets: Array<any> = [
    {data: [20, 40, 60, 80, 100], label: 'Junio'},
    {data: [20, 40, 60, 80, 100], label: 'julio'},
    {data: [20, 40, 60, 80, 100], label: 'Agosto'}
  ];*/

  public chartDatasets: Array<any> = [
    {data: [25, 40, 60, 80, 100, 23, 44, 25, 66, 30], label: 'Noviembre'}
  ];

  public chartLabels: Array<any> = [
    'Cliente1', 
    'Cliente2', 
    'Cliente3', 
    'Cliente4', 
    'Cliente5', 
    'Cliente6', 
    'Cliente7', 
    'Cliente8', 
    'Cliente9', 
    'Cliente10'
  ];

  /*public chartColors:Array<any> = [{
    backgroundColor:"#072146",
    hoverBackgroundColor:"#FF0",
    borderColor:"#0F0",
    hoverBorderColor:"#00F"
},
{
  backgroundColor:"#1464a5",
  hoverBackgroundColor:"#FF0",
  borderColor:"#0F0",
  hoverBorderColor:"#00F"
},
{
  backgroundColor:"#2bcccd",
  hoverBackgroundColor:"#FF0",
  borderColor:"#0F0",
  hoverBorderColor:"#00F"
}
  ];*/

  public chartColors:Array<any> = [{
    backgroundColor:"#072146",
    hoverBackgroundColor:"#2bcccd",
    borderColor:"#0F0",
    hoverBorderColor:"#00F"
  }
  ];

  public dateOptionsSelect: any[];
  public bulkOptionsSelect: any[];
  public showOnlyOptionsSelect: any[];
  public filterOptionsSelect: any[];

  public chartOptions: any = {
    responsive: true,
    legend: {
      labels: {
        fontColor: '#5b5f62',
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          fontColor: '#5b5f62',
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: '#5b5f62',
        }
      }]
    }
  };

  constructor() { }

  ngOnInit(): void {
  }

}
