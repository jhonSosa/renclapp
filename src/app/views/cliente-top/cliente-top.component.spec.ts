import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteTopComponent } from './cliente-top.component';

describe('ClienteTopComponent', () => {
  let component: ClienteTopComponent;
  let fixture: ComponentFixture<ClienteTopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClienteTopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
